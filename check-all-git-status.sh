#!/usr/bin/env sh

set -e

HOME=~

if [ -z "$(command -v fd)" ]; then
    echo "fd is required to run this script."
fi

case "$(uname)" in

    "Darwin")
    {
      if [ ! "$(command -v gsed)" ]; then
          echo "Error: gsed is not installed. macOS uses BSD sed by default, which is not compatible with this script. Please install GNU sed:"
          echo "> brew install gsed"
          exit 1
      fi
      alias sed=gsed
    }
    ;;

    "Linux")
        # ok
    ;;

    *)
        echo "Unsupported OS: $(uname)"
        echo "Exiting"
        exit 1
    ;;
esac

report_push_status () {
    cd "$1"
    git fetch --quiet || (echo "Error occurred while fetching in '$1'")

    repo_report="$1
"
    needs_attention=

    if [ -n "$(git status --porcelain)" ]; then
        num_changes=$(git status --porcelain | wc -l)
        repo_report="$repo_report	[1;33m$num_changes[0m uncommitted changes
"
        needs_attention=TRUE
    fi

    is_out_of_sync=$(
        git status --porcelain --branch |                   # Check the branch status
        grep -E '##.*(behind|ahead)' ||                     # See if the checked out branch needs to be synced with the remote
        echo ""                                             # If anything failed, assume it means the branch is in sync
    )

    if [ -n "$is_out_of_sync" ]; then

        report_line=$(
            printf "%s" "$is_out_of_sync" |
            sed -r 's/## ([^ ]+) \[(.+)\]/\2 \1/'  |             # Reformat the line
            sed -r 's/ahead [[:digit:]]+/\x1b[1;32m\0\x1b[0m/' |    # Color the "ahead" bit green
            sed -r 's/behind [[:digit:]]+/\x1b[1;31m\0\x1b[0m/'     # Color the "behind" bit red
        )

        repo_report="$repo_report	$report_line
"
        needs_attention=TRUE
    fi

    if [ -n "$needs_attention" ]; then
        echo "$repo_report"
    fi
}


repos=$(fd .git ~ --glob --hidden --no-ignore --type=d --exclude="/.*" | xargs -I _ dirname "_" | sort)

echo "$repos" | while read -r repo; do
    report_push_status "$repo"
done
